# PRÀCTICA 17 - SCRIPT APROVISIONAMENT VAGRANT #

En aquesta pràctica faràs servir Vagrant per automatitzar la creació d’una màquina virtual en VirtualBox amb la darrera versió d’Ubuntu Server, amb 2GB de memòria. L’hauràs de provisionar amb un script per tal que aquesta MV contingui una pila LAMP. Per controlar la base de dades també es demana que quedi instal·lada l’aplicació adminer https://github.com/vrana/adminer

## Requisitos previos

* Instalar Linux
* Instalar Vagrant

## Pasos para la instalación

Para empezar la instalación hay que descargar los archivos necesarios del repositorio de Bitbucket.

```
git clone https://AndresJordan@bitbucket.org/AndresJordan/m06-practica-17-script-aprovisionament-vagrant.git
```

Este repositorio contiene dos ficheros:

```
provision-for-apache-mysql.sh
```

### provision-for-apache-mysql.sh

Apache:
En este script se actualizan los repositorios. Se instala el apache2, las librerías necesarias de php para la comunicación con Apache. Se descarga el adminer.

Mysql:
En este script Actualiza los repositorio. Se instalan las herramientas necesarias. Se asigna como variable de entorno la contraseña de la base de datos. Se asigna la contraseña de la base de datos. Instala mysql-server. Se substituye la localhost por la IP de default router en el fichero de configuración. Reinicia el mysql. Al usuario root le da todos los permisos. Crea una base de datos llamada pruebaVagrant.

## Pasos para la ejecución

Situados en el mismo directorio ejecutamos el siguiente comando.

```
vagrant up
```

## VirtualBox


```
User: vagrant
Password: vagrant
```

Puedes acceder a la máquina virtual con la tarjeta de red Solo-anfitrión que funciona como una red privada entre la máquina real y la virtual. Puedes mirar la ip que ha generado en la máquina virtual y acceder a aprtir de ella en la máquina real.
