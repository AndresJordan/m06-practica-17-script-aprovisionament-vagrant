#!/bin/bash
# Actualizamos los repositorios
apt-get update
# Instalamos Apache
apt-get install -y apache2
# Instalamos las librerias necesarias de php para la comunicación con Mysql y Apache
apt-get install -y php libapache2-mod-php php-mysql
# Reiniciamos el servicio de Apache
sudo systemctl restart apache2
# Nos dirigimos al directorio html
cd /var/www/html
# Descargamos el adminer
wget https://github.com/vrana/adminer/releases/download/v4.3.1/adminer-4.3.1-mysql.php
# Cambimos el nombre del archivo php
mv adminer-4.3.1-mysql.php adminer.php
# Actualizamos los repositorios
apt-get update
# Instalamos las herramientas debconf
apt-get -y install debconf-utils
# Asignamos la contraseña de la base de datos
DB_ROOT_PASSWD=root
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DB_ROOT_PASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DB_ROOT_PASSWD"
# Instalamos el mysql server
apt-get install -y mysql-server
# Habilitamos que escuche todo tipo de conexion
sed -i -e 's/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf
# Reiniciamos el servicio de mysql
sudo systemctl restart mysql
# Asignamos la contraseña de root de mysql
mysql -uroot mysql -p$DB_ROOT_PASSWD <<< "GRANT ALL PRIVILEGES ON *.* TO root@'%' IDENTIFIED BY '$DB_ROOT_PASSWD'; FLUSH PRIVILEGES;"
# Creamos una base de datos de pruebas
mysql -uroot mysql -p$DB_ROOT_PASSWD <<< "CREATE DATABASE pruebaVagrant;"
